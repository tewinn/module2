<html lang="en">
<head>
	<meta charset="utf-8"/>
	<link rel="stylesheet" type="text/css" href="calculator.css" />
	<title>PHP Calculator Output</title>
</head>

<body id="mainBody">
	<p>
		<?php 
			$a = $_GET["a"]; 
			$b = $_GET["b"];
			$arith = $_GET["arithmetic"];
			
			if (!is_numeric($a))
			{
				echo "a is not a number, defaulting to a = 0 <br>";
				$a = 0; 
			}
			if (!is_numeric($b))
			{
				echo "b is not a number, defaulting to b = 0 <br>";
				$b = 0; 
			}
			
			if ($arith == "add") {
				$c = $a + $b;
				echo "$a + $b = $c <br>";
			} elseif ($arith == "subtract") {
				$c = $a - $b;
				echo "$a - $b = $c <br>";
			} elseif ($arith == "divide") {
				if ($b == 0)
				{
					
				}
				else
				{
					$c = $a / $b;
					echo "$a / $b = $c <br>";
				}
			} elseif ($arith == "multiply") {
				$c = $a * $b;
				echo "$a * $b = $c <br>";
			} else {
				echo "No arithmetic function selected! <br>";
			}
			
		?>
	</p>

</body>
</html>
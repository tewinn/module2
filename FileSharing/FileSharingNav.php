<?php
session_start();

if (isset($_POST["viewPrivateFiles"]))
{
header('Location: /~tewinn/module2_fileSharing/FileSharingPrivateFiles.php');
}
else if (isset($_POST["viewPublicFiles"]))
{
header('Location: /~tewinn/module2_fileSharing/FileSharingPublicFiles.php');
}
else if (isset($_POST["viewUploadPage"]))
{
header('Location: /~tewinn/module2_fileSharing/FileSharingUserHome.php');
}
else
{ // Logout
// Destroy session
session_unset();
session_destroy();
// Return to login page
header('Location: /~tewinn/module2_fileSharing/FileSharingLogin.html');
}

?>
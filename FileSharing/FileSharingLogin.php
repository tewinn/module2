<?php
session_start();
if(isset($_SESSION["user"])){
    unset($_SESSION["user"]);
}

$_SESSION["user"] = $_GET["username"];
$usersFile = fopen("/home/tewinn/Module2/FileSharingSite/users.txt", "r");
$valid = false;
while(!feof($usersFile)) {
                $usernameFromFile =  chop(fgets($usersFile));
                if(strcmp($_SESSION["user"],$usernameFromFile) == 0)
                {
                    // Username is valid
                    $valid = true;
                    break;
                }
                else
                {
                    $valid = false;
                }
            }
            fclose($usersFile);
            if($valid == false)
            {
                // Invalid user, go to error page

session_unset();
session_destroy();
                header('Location: /~tewinn/module2_fileSharing/FileSharingError.html');
            }
else
{
$_SESSION["userFolder"] = "/home/tewinn/Module2/FileSharingSite/" . $_SESSION["user"];
$_SESSION["publicFolder"] = "/home/tewinn/Module2/FileSharingSite/public";
header('Location: /~tewinn/module2_fileSharing/FileSharingUserHome.php');
}
?>
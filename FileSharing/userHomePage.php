<?php
session_start();
if(isset($_SESSION["user"])){
    unset($_SESSION["user"]);
}
?>

<!DOCTYPE html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="FS.css" />
        <title>File Sharing Home</title>
    </head>
    <body>
        <div id="mainUserHome">
            <?php
            $_SESSION["user"] = $_GET["username"];
            $usersFile = fopen("/home/tewinn/Module2/FileSharingSite/users.txt", "r");
            $valid = false;
            while(!feof($usersFile)) {
                $usernameFromFile =  chop(fgets($usersFile));
                if(strcmp($_SESSION["user"],$usernameFromFile) == 0)
                {
                    // Username is valid
                    $valid = true;
                    break;
                }
                else
                {
                    $valid = false;
                }
            }
            fclose($usersFile);
            if($valid == false)
            {
                // Invalid user, go to error page
                header('Location: /~tewinn/module2_fileSharing/FileSharingError.html'); 
            }
            echo "Welcome back!";
            ?>
        </div>
    </body>
</html>
    
<?php
//    session_unset(); 
//   session_destroy();
?>
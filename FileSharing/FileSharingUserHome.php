<?php
session_start();
?>

<!DOCTYPE html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="FS.css" />
<?php
echo "<title>" . $_SESSION["user"] . "'s File Sharing Home Page</title>";
?>
    </head>
    <body>
        <div id="mainUserHomeDiv">
	<div id="fileUploadDiv">
            <form id="fileuploadForm" action="FileSharingUpload.php" method="post" enctype="multipart/form-data">
                <p>
                    Select a file to upload and specify if you want the file to be shared privately (only you can view) or publicly (any user can view).<br>
                    <label for="fileName">File: </label>
		    <input type="file" name="file"/> <br>
		    <input type="radio" name="privateFile" value="privateFile"/> Private
		    <input type="radio" name="publicFile" value="publicFile"/> Public
                    <input type="submit" value="Upload">
                </p>
            </form>
</div>
<div id="fileListNav">
<form id="fileListForm" action="FileSharingNav.php" method="post">
<p>
<input type="submit" name="viewPrivateFiles" value="View Private Files">
<input type="submit" name="viewPublicFiles" value="View Public Files"></p>
<p>
<input type="submit" name="logout" value="Logout">
</p>
</form>
</div>
        </div>
    </body>
</html>